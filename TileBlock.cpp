#include "stdafx.h"
#include "TileBlock.h"

TileBlock::TileBlock(int x, int y) : Tile(TileType::BLOCK, x, y) {
	this->solid = true;
}