#pragma once
#include "FlashingTile.h"

class TileVanishVertical : public FlashingTile
{
public:
	TileVanishVertical(int x, int y);

	void update() override;

private:
	int velocity;
};

