#pragma once
#include <string>

using namespace std;

struct Rectangle {
	int x, y, width, height;

	Rectangle(int x, int y);
	Rectangle(int x, int y, int width, int height);

	bool intersects(const Rectangle &r);

	static bool intersects(int x1, int y1, int width1, int height1, int x2, int y2, int width2, int height2);

	string toString() const;
};