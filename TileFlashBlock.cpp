#include "stdafx.h"
#include "TileFlashBlock.h"
#include "Squared.h"

TileFlashBlock::TileFlashBlock(int x, int y) : FlashingTile(TileType::FLASH_BLOCK, x, y, 45) {}

bool TileFlashBlock::doesReset() {
	return false;
}

