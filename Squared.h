#pragma once
#include "Game.h"
#include "DrawUtils.h"
#include "Player.h"
#include "Image.h"

class Squared : public Game {

public:
	Squared();
	~Squared();
	void Game::update() override;
	void Game::render() override;
	void load();
	void onLoaded() override;

	void keyPressed(int keycode) override;
	void keyReleased(int keycode) override;
	void keyCharPress(unsigned int character) override;

	void mouseClick(int button) override;
	void mouseRelease(int button) override;
	void mouseMoved() override;
	void mouseScrolled(double dx, double dy) override;

	int getTileSize() { return tileSize; }
	void setTileSize(int size);
	Player* getPlayer() { return player; }

	static Squared* getInstance() { return instance; }
	static int FPS;

private:
	int tileSize = 0;
	Player* player;
	Image *loadingImage;
	static Squared* instance;
};

