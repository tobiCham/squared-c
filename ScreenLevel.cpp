#include "stdafx.h"
#include "ScreenLevel.h"
#include "Transition.h"
#include <Windows.h>

Image* ScreenLevel::pathImage = Image::fromFile("resources/path.img");

ScreenLevel::ScreenLevel(Squared* game, vector<Tile*> *tiles, int levelnumber) : Screen(game) {
	pathImage->generateTexture();
	this->tiles = tiles;
	this->levelNumber = levelnumber;
	this->levelpack = false;
	findStartPos();

	game->getPlayer()->setX(startX);
	game->getPlayer()->setY(startY);
}

ScreenLevel::~ScreenLevel() {
	while (!tiles->empty()) {
		delete tiles->back();
		tiles->pop_back();
	}
	delete tiles;
}

void ScreenLevel::findStartPos() {
	for (int i = 0; i < tiles->size(); i++) {
		Tile* tile = (*tiles)[i];
		if (&(tile->getTileType()) == &(TileType::START)) {
			startX = tile->getX();
			startY = tile->getY();
			break;
		}
	}
}

bool ScreenLevel::canTileGo(int x, int y, Tile* tile) {
	int tileSize = getGame()->getTileSize();
	if (x < 0 || y < 0 || x + tileSize > getGame()->getInitialWidth() || y + tileSize > getGame()->getInitialHeight()) return false;

	for (int i = 0; i < tiles->size(); i++) {
		Tile* t2 = tiles->operator[](i);
		if (t2 == tile) continue;
		if (!t2->isSolid()) continue;
		if (Rectangle::intersects(x, y, tileSize, tileSize, t2->getX(), t2->getY(), tileSize, tileSize)) {
			return false;
		}
	}
	return true;
}

bool ScreenLevel::canObjectGo(int x, int y) {
	int tileSize = getGame()->getTileSize();
	if (x < 0 || y < 0 || x + tileSize > getGame()->getInitialWidth() || y + tileSize > getGame()->getInitialHeight()) return false;

	for (int i = 0; i < tiles->size(); i++) {
		Tile* tile = tiles->operator[](i);
		if (!tile->isSolid()) continue;
		if (Rectangle::intersects(tile->getX(), tile->getY(), tileSize, tileSize, x, y, tileSize, tileSize)) {
			return false;
		}
	}
	return true;
}

Tile* ScreenLevel::getTile(int x, int y) {
	int tileSize = getGame()->getTileSize();

	for (int i = 0; i < tiles->size(); i++) {
		Tile* tile = this->tiles->operator[](i);
		if (Rectangle::intersects(x, y, tileSize, tileSize, tile->getX(), tile->getY(), tileSize, tileSize)) { return tile; }
	}
	return nullptr;
}

void ScreenLevel::playerDeath() {
	Player* player = getGame()->getPlayer();
	player->setX(startX);
	player->setY(startY);
	deaths++;
	Transition::fadeIn(1);
}

bool ScreenLevel::contains(vector<Point*>* points, Point& point) {
	for (int i = 0; i < points->size(); i++) {
		Point* p = points->operator[](i);
		if (p->equals(point)) return true;
	}
	return false;
}
int ScreenLevel::indexOf(vector<Point*>* points, Point& point) {
	for (int i = 0; i < points->size(); i++) {
		Point* p = points->operator[](i);
		if (p->equals(point)) return i;
	}
	return -1;
}

void ScreenLevel::render() {
	ColorUtils::setGLColor(255, 255, 255);

	glBindTexture(GL_TEXTURE_2D, pathImage->generateTexture());
	DrawUtils::fillTexture(0, 0, getGame()->getInitialWidth(), getGame()->getInitialHeight());
	glBindTexture(GL_TEXTURE_2D, 0);

	int tileSize = getGame()->getTileSize();

	int totalTiles = 0;
	for (int i = 0; i < tiles->size(); i++) {
		if (tiles->operator[](i)->isVisible()) totalTiles++;
	}

	//+1 for the player
	GLuint list = glGenLists(totalTiles + 1);

	glBegin(GL_QUADS);
	for (int i = 0; i < tiles->size(); i++) {
		Tile* tile = (*tiles)[i];
		if(tile->isVisible()) tile->render();
	}
	getGame()->getPlayer()->render();

	glEnd();
	glCallList(list);
}

void ScreenLevel::update() {
	Player* player = getGame()->getPlayer();
	int tileSize = getGame()->getTileSize();

	int size = tiles->size();
	bool hasDied = false;
	for (int i = 0; i < size; i++) {
		Tile* tile = (*tiles)[i];
		tile->update();

		if (Rectangle::intersects(player->getX(), player->getY(), tileSize, tileSize, tile->getX(), tile->getY(), tileSize, tileSize)) {
			tile->playerOnTile();
			if (tile->doesReset() && !hasDied) {
				playerDeath();
				hasDied = true;
			}
		}
	}
	player->update();

	if (Screen::getSelectedScreen() != this) delete this;
}

void ScreenLevel::screenEntered(Screen* previous) {
	Transition::fadeIn(1);
	glfwSetWindowTitle(getGame()->getWindow(), ("Squared - Level " + to_string(getLevelNumber())).c_str());
}

void ScreenLevel::screenExited(Screen* next) {
	glfwSetWindowTitle(getGame()->getWindow(), "Squared");
}

void ScreenLevel::keyPressed(int keycode) {
}