#include "stdafx.h"
#include "Transition.h"
#include "Squared.h"

int Transition::fadeInCounter = 0;
int Transition::fadeOutCounter = 0;
int Transition::maxFadeOut = 0;
int Transition::maxFadeIn = 0;

void Transition::fadeIn(float seconds) {
	int updates = (int)(seconds * Squared::FPS);
	fadeInCounter = updates;
	maxFadeIn = updates;
}

void Transition::fadeOut(float seconds) {
	int updates = (int)(seconds * Squared::FPS);
	fadeOutCounter = updates;
	maxFadeOut = updates;
}

void Transition::render() {
	if (maxFadeOut > 0 && fadeOutCounter > 0) {
		float percentage = (maxFadeOut - fadeOutCounter) / (float) maxFadeOut;
		int alpha = (int) (percentage * 255);
		ColorUtils::setGLColor(0, 0, 0, alpha);
		DrawUtils::fillRect(0, 0, Squared::getInstance()->getInitialWidth(), Squared::getInstance()->getInitialHeight());
	}

	if (maxFadeIn > 0 && fadeInCounter > 0) {
		float percentage = (maxFadeIn - fadeInCounter) / (float) fadeInCounter;
		int alpha = (int) (255 - (percentage * 255));
		ColorUtils::setGLColor(0, 0, 0, alpha);
		DrawUtils::fillRect(0, 0, Squared::getInstance()->getInitialWidth(), Squared::getInstance()->getInitialHeight());
	}
}

void Transition::update() {
	if (maxFadeOut > 0 && fadeOutCounter > 0) fadeOutCounter--;
	if (maxFadeIn > 0 && fadeInCounter > 0) fadeInCounter--;
}

void Transition::clearTransitions() {
	fadeInCounter = 0;
	fadeOutCounter = 0;
	maxFadeOut = 0;
	maxFadeIn = 0;
}