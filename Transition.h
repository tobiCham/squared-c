#pragma once
#include <vector>

class Transition
{
private:
	Transition();
	static int fadeOutCounter, maxFadeOut;
	static int fadeInCounter, maxFadeIn;

public:
	static void fadeIn(float seconds);
	static void fadeOut(float seconds);
	static void render();
	static void update();
	static void clearTransitions();
};

