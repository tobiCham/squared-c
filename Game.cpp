#include "stdafx.h"
#include "Game.h"

std::vector<Game*> Game::games;

Game::Game()
{
	glfwInit();
	glfwSetErrorCallback(Game::onError);
	Game::games.push_back(this);
}

Game::~Game()
{
	delete mouse;
	delete keyboard;
}

void Game::init(int width, int height, string title)
{
	this->width = width;
	this->height = height;
	this->title = title;
	this->renderWidth = width;
	this->renderHeight = height;
	
	const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	int maxWidth = (int)(mode->width / 1.2);
	int maxHeight = (int)(mode->height / 1.2);

	float aspectRatio = (float) width / height;
	if (width > maxWidth) {
		width = maxWidth;
		height = (int)(width * aspectRatio);
	}
	if (height > maxHeight) {
		height = maxHeight;
		width = (int)(height / aspectRatio);
	}

	//Fullscreen
	//glfwWindowHint(GLFW_RED_BITS, mode->redBits);
	//glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
	//glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
	//glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
	//this->window = glfwCreateWindow(mode->width, mode->height, title.c_str(), glfwGetPrimaryMonitor(), NULL);

	this->window = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
	
	glfwSetWindowPos(window, (mode->width - width) / 2, (mode->height - height) / 2);
	this->mouse = new Mouse(this);
	this->keyboard = new Keyboard(this);
}

Game* Game::getGame(GLFWwindow* window) {
	for (int i = 0; i < games.size(); i++) {
		if (games[i]->window == window) return games[i];
	}
	return nullptr;
}

void Game::windowResize(GLFWwindow* window, int width, int height) {
	Game* game = getGame(window);
	if (game == nullptr) return;
	if (window != game->window) return;

	double scaleFactor = (float)width / game->getInitialWidth();
	int newHeight = (int)(game->getInitialHeight() * scaleFactor);
	if (newHeight > height) scaleFactor = (float)height / game->getInitialHeight();
	int newWidth = (int)(scaleFactor * game->getInitialWidth());
	newHeight = (int)(scaleFactor * game->getInitialHeight());

	int renderX = (width / 2) - (newWidth / 2);
	int renderY = (height / 2) - (newHeight / 2);

	glViewport(renderX, renderY, newWidth, newHeight);
	game->renderX = renderX;
	game->renderY = renderY;
	game->renderWidth = newWidth;
	game->renderHeight = newHeight;

	game->onWindowResized();
}

void Game::open() {
	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, Keyboard::keyEvent);
	glfwSetCharCallback(window, Keyboard::keyCharEvent);
	glfwSetMouseButtonCallback(window, Mouse::mouseButtonEvent);
	glfwSetCursorPosCallback(window, Mouse::mouseMove);
	glfwSetScrollCallback(window, Mouse::mouseScrollEvent);

	glfwSetWindowSizeCallback(window, Game::windowResize);

	glViewport(0, 0, width, height);
	glOrtho(0, width, height, 0, 0, 1);

	int width, height;
	glfwGetWindowSize(window, &width, &height);
	windowResize(window, width, height);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);

	glClearColor(0, 0, 0, 1);
	onLoaded();

	while (!glfwWindowShouldClose(this->window))
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glfwPollEvents();

		update();
		render();

		glfwSwapBuffers(window);
	}

	glfwTerminate();
}

void Game::onError(int id, const char *message) {
	std::cerr << "Error: " << id << ": " << message << endl;
}