#include "stdafx.h"
#include "Rectangle.h"

Rectangle::Rectangle(int x, int y) {
	this->x = x;
	this->y = y;
}

Rectangle::Rectangle(int x, int y, int width, int height) {
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
}

bool Rectangle::intersects(const Rectangle & r) {
	return Rectangle::intersects(x, y, width, height, r.x, r.y, r.width, r.height);
}

bool Rectangle::intersects(int x1, int y1, int width1, int height1, int x2, int y2, int width2, int height2) {
	if (width1 <= 0 || width2 <= 0 || height1 <= 0 || height2 <= 0) return false;
	width1 += x1;
	height1 += y1;
	width2 += x2;
	height2 += y2;
	return ((width2 < x2 || width2 > x1) && (height2 < y2 || height2 > y1) && (width1 < x1 || width1 > x2) && (height1 < y1 || height1 > y2));
}

string Rectangle::toString() const {
	return "(" + to_string(x) + ", " + to_string(y) + ", " + to_string(width) + ", " + to_string(height) + ")";
}