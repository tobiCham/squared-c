#include "stdafx.h"
#include "ScreenLevel.h"
#include "Squared.h"

vector<Tile*>* ScreenLevel::getTiles(Image* image) {
	vector<Tile*>* tiles = new vector<Tile*>();
	
	Squared* game = Squared::getInstance();
	game->setTileSize(game->getInitialWidth() / image->getWidth());
	int size = game->getTileSize();
		
	for (int j = 0; j < image->getHeight(); j++) {
		for (int i = 0; i < image->getWidth(); i++) {
			int color = image->getPixel(i, j);
			const TileType* tileType = TileType::getTileType(color);
			if (tileType == nullptr) tileType = &TileType::PATH;

			Tile* tile = Tile::createTile(tileType, i * size, j * size);

			if (tile != nullptr) tiles->push_back(tile);
		}
	}
	return tiles;
}

ScreenLevel* ScreenLevel::loadLevel(int id) {
	Image* image = Image::fromFile("resources/levels/level" + to_string(id) + ".lvl");
	if (image == nullptr) return nullptr;

	vector<Tile*>* tiles = getTiles(image);
	if (tiles == nullptr) {
		delete image;
		return nullptr;
	}

	return new ScreenLevel(Squared::getInstance(), tiles, id);
}