#include "stdafx.h"
#include "TileVerticalMove.h"
#include "Squared.h"
#include "ScreenLevel.h"

TileVerticalMove::TileVerticalMove(int x, int y) : Tile(TileType::VERTICAL_MOVE, x, y) {
	this->velocity = (int)((3.0 / (Squared::FPS / 30.0)) * (Squared::getInstance()->getTileSize() / 32.0));
	this->solid = true;
	this->resets = true;
}

void TileVerticalMove::update() {
	int newPos = getY() + velocity;
	ScreenLevel* level = (ScreenLevel*)Screen::getSelectedScreen();
	if (level->canTileGo(getX(), newPos, this)) y = newPos;
	else velocity *= -1;
}
