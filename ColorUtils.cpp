#include "stdafx.h"
#include "ColorUtils.h"

int ColorUtils::getColor(int red, int green, int blue, int alpha) {
	red = red & 255;
	green = green & 255;
	blue = blue & 255;
	alpha = alpha & 255;
	return (alpha << 24) | (red << 16) | (green << 8) | (blue << 0);
}

int ColorUtils::getColor(int red, int green, int blue) {
	return getColor(red, green, blue, 255);
}

int ColorUtils::getRed(int color) {
	return (color >> 16) & 0xFF;
}

int ColorUtils::getGreen(int color) {
	return (color >> 8) & 0xFF;
}

int ColorUtils::getBlue(int color) {
	return (color >> 0) & 0xFF;
}

int ColorUtils::getAlpha(int color) {
	return (color >> 24) & 0xff;
}

float ColorUtils::getRedf(int color) {
	return (float) getRed(color) / 256.0;
}

float ColorUtils::getGreenf(int color) {
	return (float)getGreen(color) / 256.0;
}

float ColorUtils::getBluef(int pixel) {
	return (float)getBlue(pixel) / 256.0;
}

float ColorUtils::getAlphaf(int color) {
	return (float)getAlpha(color) / 256.0;
}

void ColorUtils::setGLColor(int color) {
	glColor4f(getRedf(color), getGreenf(color), getBluef(color), getAlphaf(color));
}

void ColorUtils::setGLColor(int red, int green, int blue) {
	glColor3f(red / 255.0, green / 255.0, blue / 255.0);
}

void ColorUtils::setGLColor(int red, int green, int blue, int alpha) {
	glColor4f(red / 255.0, green / 255.0, blue / 255.0, alpha / 255.0);
}