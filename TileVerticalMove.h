#pragma once
#include "Tile.h"
class TileVerticalMove : public Tile {
public:
	TileVerticalMove(int x, int y);
	void update() override;
private:
	int velocity;
};

