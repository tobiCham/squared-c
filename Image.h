#pragma once

#include "ColorUtils.h"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

/**
* Image format:
* 1 byte = type. 0 for non alpha, 1 for alpha
* int -> width
* int -> height
* following bytes in either RGB or RGBA format
*/
class Image
{
public:
	Image(int width, int height, bool alpha);
	~Image();

	inline int getWidth() { return width;  }
	inline int getHeight() { return height;  }
	inline bool hasAlpha() { return alpha; }
	inline GLubyte *getData() { return data; }

	int getPixel(int x, int y);

	GLuint generateTexture();

	GLFWimage* createGLFWImage();

	static Image* fromFile(string file);
	static Image* fromFileAndGen(string file);

private:
	GLuint textureID = 0;

	int width, height;
	bool alpha;

	GLubyte* data;

	static int readInt(ifstream* stream);
	static Image* fromData(int width, int height, char* data, bool alpha);
};

