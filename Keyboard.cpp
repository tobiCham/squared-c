#include "stdafx.h"
#include "Keyboard.h"
#include <iostream>
#include "Game.h"

Keyboard::Keyboard(Game* game) {
	this->game = game;
}

bool Keyboard::isKeyDown(int keycode) {
	return glfwGetKey(game->getWindow(), keycode) != GLFW_RELEASE;
}

void Keyboard::keyEvent(GLFWwindow * window, int key, int scancode, int action, int mods) {
	if (action == GLFW_RELEASE) {
		Game* game = Game::getGame(window);
		if (game != nullptr) game->keyReleased(key);
	} else if (action == GLFW_PRESS || action == GLFW_REPEAT) {
		Game* game = Game::getGame(window);
		if (game != nullptr) game->keyPressed(key);
	}
	else {
		cout << "Unknown id " << action << endl;
	}
}

void Keyboard::keyCharEvent(GLFWwindow *window, unsigned int character) {
	Game* game = Game::getGame(window);
	if (game != nullptr) game->keyCharPress(character);
}
