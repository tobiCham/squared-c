#include "stdafx.h"
#include "TileVanishHorizontal.h"
#include "Squared.h"
#include "ScreenLevel.h"

TileVanishHorizontal::TileVanishHorizontal(int x, int y) : FlashingTile(TileType::VANISH_HORIZONTAL_MOVE, x, y, 45) {
	this->velocity = 3 * Squared::getInstance()->getTileSize() / 32;
}

void TileVanishHorizontal::update() {
	FlashingTile::update();

	int newPos = getX() + velocity;
	ScreenLevel* level = (ScreenLevel*)Screen::getSelectedScreen();
	if (level->canTileGo(newPos, getY(), this)) x = newPos;
	else velocity *= -1;
}