#include "stdafx.h"
#include "Player.h"
#include "Squared.h"
#include "ScreenLevel.h"

Player::Player(Squared* game) {
	this->game = game;
	updateVelocity();
}

void Player::render() {
	ColorUtils::setGLColor(0, 0, 255);
	int tileSize = game->getTileSize();

	glVertex2i(x, y);
	glVertex2i(x + tileSize, y);
	glVertex2i(x + tileSize, y + tileSize);
	glVertex2i(x, y + tileSize);
}

void Player::update() {
	updateVelocity();
	ScreenLevel* level = (ScreenLevel*) Screen::getSelectedScreen();
	double newX = x;
	double newY = y;

	Keyboard* keyboard = game->getKeyboard();
	if (keyboard->isKeyDown(GLFW_KEY_LEFT)) newX -= velX;
	if (keyboard->isKeyDown(GLFW_KEY_RIGHT)) newX += velX;
	if (keyboard->isKeyDown(GLFW_KEY_UP)) newY -= velY;
	if (keyboard->isKeyDown(GLFW_KEY_DOWN)) newY += velY;

	if (level->canObjectGo((int) newX, (int)y)) x = newX;
	if (level->canObjectGo((int) x, (int)newY)) y = newY;
}

void Player::updateVelocity() {
	velX = game->getTileSize() / ((Squared::FPS / 30.0) * 8.0);
	velY = game->getTileSize() / ((Squared::FPS / 30.0) * 8.0);
}
