#include "stdafx.h"
#include "DrawUtils.h"

void DrawUtils::fillRect(float x, float y, float width, float height) {
	glBegin(GL_QUADS);
	glVertex2f(x, y);
	glVertex2f(x + width, y);
	glVertex2f(x + width, y + height);
	glVertex2f(x, y + height);
	glEnd();
}

void DrawUtils::fillTexture(float x, float y, float width, float height) {
	glBegin(GL_QUADS);

	glTexCoord2f(0, 0);
	glVertex2f(x, y);

	glTexCoord2f(1, 0);
	glVertex2f(x + width, y);

	glTexCoord2f(1, 1);
	glVertex2f(x + width, y + height);

	glTexCoord2f(0, 1);
	glVertex2f(x, y + height);

	glEnd();
}
