#pragma once
#include "Squared.h"

class Screen
{
public:
	Squared* getGame();
	Screen(Squared* game);
	
	virtual void update() = 0;
	virtual void render() = 0;

	virtual void screenEntered(Screen* lastScreen) {}
	virtual void screenExited(Screen* nextScreen) {}
	
	virtual void keyPressed(int key) {}
	virtual void keyReleased(int key) {}
	virtual void keyCharPress(unsigned int character) {}

	virtual void mouseClick(int button) {}
	virtual void mouseRelease(int button) {}
	virtual void mouseMoved() {}
	virtual void mouseScroll(double dx, double dy) {}

	static void setSelectedScreen(Screen* screen);
	static Screen* getSelectedScreen() { return selected; }

private:
	Squared* game;
	
	static Screen* selected;
};

