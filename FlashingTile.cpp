#include "stdafx.h"
#include "FlashingTile.h"
#include "Squared.h"

FlashingTile::FlashingTile(const TileType & type, int x, int y, int frequency) : Tile(type, x, y) {
	this->counter = rand() % frequency;
	this->frequency = frequency;
	this->hidden = false;
}

void FlashingTile::update() {
	counter--;
	if (counter <= 0) {
		counter = frequency;
		hidden = !hidden;
	}
}

int FlashingTile::getColor() {
	return ColorUtils::getColor(ColorUtils::getRed(color), ColorUtils::getGreen(color), ColorUtils::getBlue(color), hidden ? hiddenTransparency : 255);
}

bool FlashingTile::isSolid() {
	return !hidden;
}

bool FlashingTile::doesReset() {
	return !hidden;
}