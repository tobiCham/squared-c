#pragma once

class Squared;

class Player
{
public:
	Player(Squared *game);
	void updateVelocity();
	void render();
	void update();

	float getX() { return x; }
	float getY() { return y; }
	float getVelX() { return velX; }
	float getVelY() { return velY; }

	void setX(float x) { this->x = x; }
	void setY(float y) { this->y = y; }
	void setVelX(float velx) { this->velX = velx; }
	void setVelY(float vely) { this->velY = vely; }

private:
	float x, y;
	float velX, velY;
	Squared* game;
};

