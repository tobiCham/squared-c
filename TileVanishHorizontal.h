#pragma once
#include "FlashingTile.h"

class TileVanishHorizontal : public FlashingTile
{
public:
	TileVanishHorizontal(int x, int y);
	void update() override;

private:
	int velocity;
};

