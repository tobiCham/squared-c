#include "stdafx.h"
#include "Point.h"


Point::Point(int x, int y) {
	this->x = x;
	this->y = y;
}

bool Point::equals(Point& point2) {
	return x == point2.x && y == point2.y;
}