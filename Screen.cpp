#include "stdafx.h"
#include "Screen.h"
#include "Transition.h"

Screen* Screen::selected = nullptr;

Screen::Screen(Squared* game) {
	this->game = game;
}

void Screen::setSelectedScreen(Screen* screen) {
	Transition::clearTransitions();
		
	Screen* old = Screen::selected;
	if (selected != nullptr) selected->screenExited(screen); 
	Screen::selected = screen;
	if (selected != nullptr) selected->screenEntered(old);
}

Squared* Screen::getGame() {
	return this->game;
}