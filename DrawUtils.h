#pragma once

#include "GLFW/glfw3.h"

class DrawUtils
{
public:
	static void fillRect(float x, float y, float width, float height);
	static void fillTexture(float x, float y, float width, float height);
};

