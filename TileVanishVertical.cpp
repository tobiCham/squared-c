#include "stdafx.h"
#include "TileVanishVertical.h"
#include "Squared.h"
#include "ScreenLevel.h"

TileVanishVertical::TileVanishVertical(int x, int y) : FlashingTile(TileType::VANISH_VERTICAL_MOVE, x, y, 45) {
	this->velocity = 3 * Squared::getInstance()->getTileSize() / 32;
}

void TileVanishVertical::update() {
	FlashingTile::update();

	int newPos = getY() + velocity;
	ScreenLevel* level = (ScreenLevel*)Screen::getSelectedScreen();
	if (level->canTileGo(getX(), newPos, this)) y = newPos;
	else velocity *= -1;
}