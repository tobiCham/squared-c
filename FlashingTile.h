#pragma once

#include "Tile.h"

class FlashingTile : public Tile {
public:
	FlashingTile(const TileType& type, int x, int y, int frequency);

	void update() override;
	int getColor() override;
	bool isSolid() override;
	bool doesReset() override;

private:
	int frequency;

//protected:
	bool hidden;
	int counter = 0;
	int hiddenTransparency = 100;


};

