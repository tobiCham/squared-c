#include "stdafx.h"
#include "Image.h"

Image::Image(int width, int height, bool alpha) : width(width), height(height), alpha(alpha) {
	this->data = new GLubyte[width * height * (alpha ? 4 : 3)];
}

int Image::getPixel(int x, int y) {
	if (alpha) {
		int startPos = ((y * width) + x) * 4;
		return ColorUtils::getColor(data[startPos], data[startPos + 1], data[startPos + 2], data[startPos + 3]);
	}
	int startPos = ((y * width) + x) * 3;
	return ColorUtils::getColor(data[startPos], data[startPos + 1], data[startPos + 2]);
}

Image* Image::fromFile(string file) {
	ifstream stream(file, ios::in | ios::binary | ios::ate);
	if (!stream.good()) return nullptr;

	stream.seekg(0, ifstream::beg); //Go to the beginning of the file

	char typeBuffer[1];
	stream.read(typeBuffer, 1);

	bool alpha = typeBuffer[0] == 1;

	int width = readInt(&stream);
	int height = readInt(&stream);

	if (width < 1) width = 1;
	if (height < 1) height = 1;

	int size = width * height * (alpha ? 4 : 3);

	char *data = new char[size];
	stream.read(data, size);

	Image* image = fromData(width, height, data, alpha);
	delete[] data;
	return image;
}

Image* Image::fromFileAndGen(string file) {
	Image* img = fromFile(file);
	if (img != nullptr) img->generateTexture();
	return img;
}

Image* Image::fromData(int width, int height, char *data, bool alpha) {
	Image *img = new Image(width, height, alpha);
	int size = width * height * (alpha ? 4 : 3);

	for (int i = 0; i < size; i++) {
		img->data[i] = data[i];
	}
	return img;
}

int Image::readInt(ifstream* stream) {
	char bytes[4];
	stream->read(bytes, 4);
	unsigned char b1 = bytes[0];
	unsigned char b2 = bytes[1];
	unsigned char b3 = bytes[2];
	unsigned char b4 = bytes[3];
	return (b1 << 24) | (b2 << 16) | (b3 << 8) | (b4);
}

GLuint Image::generateTexture() {
	if (textureID > 0) return textureID;
	GLuint textures[1];
	glGenTextures(1, textures);
	GLuint tID = textures[0];
	this->textureID = tID;

	glBindTexture(GL_TEXTURE_2D, tID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, getWidth(), getHeight(), 0, alpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, data);

	glBindTexture(GL_TEXTURE_2D, 0);

	return tID;
}

GLFWimage* Image::createGLFWImage() {
	GLFWimage* img = new GLFWimage();
	img->width = width;
	img->height = height;
	unsigned char *pixels = new unsigned char[width * height * 4];

	int counter = 0;
	for (int j = 0; j < height; j++) {
		for (int i = 0; i < width; i++) {
			int pixel = getPixel(i, j);
			pixels[counter] = ColorUtils::getRed(pixel);
			pixels[counter + 1] = ColorUtils::getGreen(pixel);
			pixels[counter + 2] = ColorUtils::getBlue(pixel);
			pixels[counter + 3] = ColorUtils::getAlpha(pixel);

			counter += 4;
		}
	}
	img->pixels = pixels;
	return img;
}

Image::~Image() {
	if (textureID > 0) glDeleteTextures(1, &textureID);
	delete[] data;
}