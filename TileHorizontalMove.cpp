#include "stdafx.h"
#include "TileHorizontalMove.h"
#include "Squared.h"
#include "ScreenLevel.h"

TileHorizontalMove::TileHorizontalMove(int x, int y) : Tile(TileType::HORIZONTAL_MOVE, x, y) {
	this->velocity = (int)((3.0 / (Squared::FPS / 30.0)) * (Squared::getInstance()->getTileSize() / 32.0));
	this->solid = true;
	this->resets = true;
}

void TileHorizontalMove::update() {
	int newPos = getX() + velocity;
	ScreenLevel* level = (ScreenLevel*) Screen::getSelectedScreen();
	if(level->canTileGo(newPos, getY(), this)) x = newPos;
	else velocity *= -1;
}