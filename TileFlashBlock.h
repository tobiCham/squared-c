#pragma once
#include "FlashingTile.h"

class TileFlashBlock : public FlashingTile
{
public:
	TileFlashBlock(int x, int y);
	
	bool doesReset() override;

private:
	int counter = 0;
};

