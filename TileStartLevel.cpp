#include "stdafx.h"
#include "TileStartLevel.h"

TileStartLevel::TileStartLevel(int x, int y) : Tile(TileType::START, x, y) {
	this->visible = false;
}