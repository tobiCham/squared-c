#pragma once
#include "TileType.h"

class Tile
{
public:
	Tile(const TileType& type, int x, int y);

	virtual void render(); 
	virtual void update() {}
	virtual void playerOnTile() {}

	int getX();
	int getY();
	virtual int getColor();
	virtual bool isSolid();
	virtual bool doesReset();
	virtual bool isVisible();

	const TileType& getTileType();

	static Tile* createTile(const TileType* type, int x, int y);

private:
	const TileType& tileType;
protected:
	int x, y, color;
	bool solid = false, resets = false, visible = true;
};

