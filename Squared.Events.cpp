#include "stdafx.h"
#include "Squared.h"
#include "Screen.h"

void Squared::keyPressed(int keycode) {
	if (Screen::getSelectedScreen() != nullptr) Screen::getSelectedScreen()->keyPressed(keycode);
}

void Squared::keyReleased(int keycode) {
	if (Screen::getSelectedScreen() != nullptr) Screen::getSelectedScreen()->keyReleased(keycode);
}

void Squared::keyCharPress(unsigned int character) {
	if (Screen::getSelectedScreen() != nullptr) Screen::getSelectedScreen()->keyCharPress(character);
}

void Squared::mouseClick(int button) {
	if (Screen::getSelectedScreen() != nullptr) Screen::getSelectedScreen()->mouseClick(button);
}

void Squared::mouseRelease(int button) {
	if (Screen::getSelectedScreen() != nullptr) Screen::getSelectedScreen()->mouseRelease(button);
}

void Squared::mouseScrolled(double dx, double dy) {
	if (Screen::getSelectedScreen() != nullptr) Screen::getSelectedScreen()->mouseScroll(dx, dy);
}

void Squared::mouseMoved() {
	if (Screen::getSelectedScreen() != nullptr) Screen::getSelectedScreen()->mouseMoved();
}