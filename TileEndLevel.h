#pragma once
#include "Tile.h"
class TileEndLevel : public Tile
{
public:
	TileEndLevel(int x, int y);
	void playerOnTile() override;
};

