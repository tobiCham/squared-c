#pragma once
#include "Screen.h"
#include "Tile.h"
#include "Rectangle.h"
#include "Point.h"
#include "Image.h"

class ScreenLevel : public Screen
{
public:
	ScreenLevel(Squared* game, vector<Tile*> *tiles, int levelnumber);
	~ScreenLevel();

	void screenEntered(Screen* previous) override;
	void screenExited(Screen* next) override;
	void render() override;
	void update() override;
	void keyPressed(int keycode) override;
	void playerDeath();
	bool canObjectGo(int x, int y);
	bool canTileGo(int x, int y, Tile* tile);
	Tile* getTile(int x, int y);
	vector<Tile*>* getTiles() { return tiles; }
	int getLevelNumber() { return levelNumber; }
	bool isLevelPack() { return levelpack; }
	float getTime() { return elapsedTime; }

	static ScreenLevel* loadLevel(int id);
	static vector<Tile*>* getTiles(Image* image);

private:
	vector<Tile*> *tiles;
	int startX, startY;
	int levelNumber;
	bool levelpack;
	float elapsedTime;
	int deaths;

	void findStartPos();
	static bool contains(vector<Point*>* points, Point& point);
	static int indexOf(vector<Point*>* points, Point& point);
	static Image* pathImage;
};

