#pragma once
#include <string>
#include <vector>

using namespace std;

class TileType {
	private:
		TileType(int color, string name, bool _static);
		const int color;
		const string name;
		const bool _static;
		static vector<TileType*> tiles;

	public:
		static const TileType START;
		static const TileType END;
		static const TileType BLOCK;
		static const TileType PATH;
		static const TileType VANISH;
		static const TileType RESET;
		static const TileType HORIZONTAL_MOVE;
		static const TileType VERTICAL_MOVE;
		static const TileType FLASH_BLOCK;
		static const TileType VANISH_HORIZONTAL_MOVE;
		static const TileType VANISH_VERTICAL_MOVE;

		static TileType* getTileType(int color);

		int getColor() const { return color; }
		string getName() const { return name; }
		bool isStatic() const { return _static; }
};

