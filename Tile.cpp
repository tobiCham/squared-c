#include "stdafx.h"
#include "Tile.h"
#include "GLFW\glfw3.h"
#include "ColorUtils.h"
#include "DrawUtils.h"
#include "Squared.h"
#include "TileBlock.h"
#include "TileEndLevel.h"
#include "TileFlashBlock.h"
#include "TileHorizontalMove.h"
#include "TilePath.h"
#include "TileReset.h"
#include "TileStartLevel.h"
#include "TileVanish.h"
#include "TileVanishHorizontal.h"
#include "TileVanishVertical.h"
#include "TileVerticalMove.h"

Tile::Tile(const TileType& type, int x, int y) : tileType(type) {
	this-> x = x;
	this-> y = y;
	this->color = type.getColor();
}

void Tile::render() {
	int tileSize = Squared::getInstance()->getTileSize();
	ColorUtils::setGLColor(getColor());

	glVertex2i(x, y);
	glVertex2i(x + tileSize, y);
	glVertex2i(x + tileSize, y + tileSize);
	glVertex2i(x, y + tileSize);
}

int Tile::getX() { return this->x; }
int Tile::getY() { return this->y; }

int Tile::getColor() { return this->color; }
bool Tile::isSolid() { return this->solid; }
bool Tile::doesReset() { return this->resets; }
bool Tile::isVisible() { return this->visible; }

const TileType& Tile::getTileType() { return this->tileType; }

Tile* Tile::createTile(const TileType* type, int x, int y) {
	if (type == &TileType::BLOCK) return new TileBlock(x, y);
	if (type == &TileType::END) return new TileEndLevel(x, y);
	if (type == &TileType::FLASH_BLOCK) return new TileFlashBlock(x, y);
	if (type == &TileType::HORIZONTAL_MOVE) return new TileHorizontalMove(x, y);
	if (type == &TileType::PATH) return new TilePath(x, y);
	if (type == &TileType::RESET) return new TileReset(x, y);
	if (type == &TileType::START) return new TileStartLevel(x, y);
	if (type == &TileType::VANISH) return new TileVanish(x, y);
	if (type == &TileType::VANISH_HORIZONTAL_MOVE) return new TileVanishHorizontal(x, y);
	if (type == &TileType::VANISH_VERTICAL_MOVE) return new TileVanishVertical(x, y);
	if (type == &TileType::VERTICAL_MOVE) return new TileVerticalMove(x, y);
	return nullptr;
}
