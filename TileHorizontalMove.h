#pragma once
#include "Tile.h"
#include "Squared.h"

class TileHorizontalMove : public Tile {
public:
	TileHorizontalMove(int x, int y);
	void update() override;
private:
	int velocity;
};

