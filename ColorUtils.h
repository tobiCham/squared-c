#pragma once

#include "GLFW\glfw3.h"

class ColorUtils
{
public:
	static int getColor(int red, int green, int blue, int alpha);
	static int getColor(int red, int green, int blue);

	static int getRed(int color);
	static int getGreen(int color);
	static int getBlue(int color);
	static int getAlpha(int color);

	static float getRedf(int color);
	static float getGreenf(int color);
	static float getBluef(int color);
	static float getAlphaf(int color);

	static void setGLColor(int color);
	static void setGLColor(int red, int green, int blue);
	static void setGLColor(int red, int green, int blue, int alpha);
};

