#include "stdafx.h"
#include "TileType.h"
#include "ColorUtils.h"

vector<TileType*> TileType::tiles;

TileType::TileType(int color, string name, bool _static) : color(color), name(name), _static(_static) {
	TileType::tiles.push_back(this);
}

const TileType TileType::START = TileType(ColorUtils::getColor(0, 0, 255), "Start", true);
const TileType TileType::END = TileType(ColorUtils::getColor(255, 0, 0), "End", true);
const TileType TileType::BLOCK = TileType(ColorUtils::getColor(0, 0, 0), "Block", true);
const TileType TileType::PATH = TileType(ColorUtils::getColor(255, 255, 255), "Path", true);
const TileType TileType::VANISH = TileType(ColorUtils::getColor(0, 255, 0), "Vanish", false);
const TileType TileType::RESET = TileType(ColorUtils::getColor(255, 255, 0), "Reset", true);
const TileType TileType::HORIZONTAL_MOVE = TileType(ColorUtils::getColor(255, 0, 255), "Horizontal Move", false);
const TileType TileType::VERTICAL_MOVE = TileType(ColorUtils::getColor(0, 255, 255), "Vertical Move", false);
const TileType TileType::FLASH_BLOCK = TileType(ColorUtils::getColor(100, 100, 100), "Flash Block", false);
const TileType TileType::VANISH_HORIZONTAL_MOVE = TileType(ColorUtils::getColor(110, 0, 110), "Vanishing Horizontal", false);
const TileType TileType::VANISH_VERTICAL_MOVE = TileType(ColorUtils::getColor(255, 110, 0), "Vanishing Vertical", false);

TileType* TileType::getTileType(int color) {
	for (int i = 0; i < tiles.size(); i++) {
		TileType* type = tiles[i];
		if (type->getColor() == color) return type;
	}
	return nullptr;
}