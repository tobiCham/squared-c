#include "stdafx.h"
#include "TilePath.h"

TilePath::TilePath(int x, int y) : Tile(TileType::PATH, x, y) {
	this->solid = false;
	this->visible = false;
}

