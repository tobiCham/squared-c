#include "stdafx.h"
#include "Mouse.h"
#include "Game.h"

Mouse::Mouse(Game *game) {
	this->game = game;
}

void Mouse::mouseMove(GLFWwindow * window, double x, double y) {
	Mouse* mouse = getMouse(window);
	if (mouse == nullptr) return;
	mouse->game->mouseMoved();
}

void Mouse::mouseButtonEvent(GLFWwindow * window, int button, int action, int mods) {
	Mouse* mouse = getMouse(window);
	if (mouse == nullptr) return;

	if (action == GLFW_RELEASE) {
		mouse->game->mouseRelease(button);
	}
	else if (action == GLFW_PRESS || action == GLFW_REPEAT) {
		mouse->game->mouseClick(button);
	}
}

void Mouse::mouseScrollEvent(GLFWwindow * window, double dx, double dy) {
	Mouse* mouse = getMouse(window);
	if (mouse == nullptr) return;
	mouse->game->mouseScrolled(dx, dy);
}

double Mouse::getExactX() {
	double x, y;
	glfwGetCursorPos(game->getWindow(), &x, &y);
	return (int) x;
}

double Mouse::getExactY() {
	double x, y;
	glfwGetCursorPos(game->getWindow(), &x, &y);
	return (int)y;
}

int Mouse::getX() {
	double relativeX = (double) getExactX() - game->getRenderX();
	double ratio = (double)game->getRenderWidth() / game->getInitialWidth();
	return (int)(relativeX / ratio);
}

int Mouse::getY() {
	double relativeY = (double) getExactY() - game->getRenderY();
	double ratio = (double) game->getRenderHeight() / game->getInitialHeight();
	return (int)(relativeY / ratio);
}

Mouse* Mouse::getMouse(GLFWwindow* window) {
	Game* game = Game::getGame(window);
	if (game == nullptr) return nullptr;
	return game->getMouse();
}


