#pragma once
#include "GLFW/glfw3.h"
#include <set>

using namespace std;

class Game;

class Keyboard
{
private:
	Game* game;
	//static set<int> keysDown;

public:
	Keyboard(Game* game);
	bool isKeyDown(int keycode);
	//static void releaseKey(int keycode);
	//static void pushKey(int keycode);
	static void keyEvent(GLFWwindow* window, int key, int scancode, int action, int mods);
	static void keyCharEvent(GLFWwindow *window, unsigned int character);
};

