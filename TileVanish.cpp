#include "stdafx.h"
#include "TileVanish.h"
#include "Squared.h"

TileVanish::TileVanish(int x, int y) : FlashingTile(TileType::VANISH, x, y, 45) {}