#pragma once
#include "GLFW/glfw3.h"
#include <string>
#include <iostream>
#include <vector>
#include "Keyboard.h"
#include "Mouse.h"

using namespace std;

class Game
{
public:
	~Game();
	void init(int width, int height, string title);
	void open();

	virtual void onLoaded() {}
	virtual void onWindowResized() {}
	virtual void keyPressed(int keycode) {}
	virtual void keyReleased(int keycode) {}
	virtual void keyCharPress(unsigned int character) {}

	virtual void mouseClick(int button) {}
	virtual void mouseRelease(int button) {}
	virtual void mouseMoved() {}
	virtual void mouseScrolled(double dx, double dy) {}

	virtual void update() = 0;
	virtual void render() = 0;

	static void onError(int errorID, const char* message);

	GLFWwindow *getWindow() { return window; }
	Mouse* getMouse() { return mouse; }
	Keyboard* getKeyboard() { return keyboard; }

	int getInitialWidth() { return width; }
	int getInitialHeight() { return height; }
	int getRenderX() { return renderX; }
	int getRenderY() { return renderY; }
	int getRenderWidth() { return renderWidth; }
	int getRenderHeight() { return renderHeight; }

	static vector<Game*> getGames() { return games; }

	static Game* getGame(GLFWwindow* window);

private:
	static vector<Game*> games;
	static void windowResize(GLFWwindow* window, int width, int height);

	GLFWwindow *window;
	Mouse* mouse;
	Keyboard* keyboard;

	int width, height;
	int clearColor = 0xffffff;
	string title;

	int renderX = 0, renderY = 0, renderWidth = 0, renderHeight = 0;
	int updates = 0;
	double lastSeconds = 0;
protected:
	Game();
};