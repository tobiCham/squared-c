#include "stdafx.h"
#include "TileEndLevel.h"

#include "ScreenLevel.h"

TileEndLevel::TileEndLevel(int x, int y) : Tile(TileType::END, x, y) {}

void TileEndLevel::playerOnTile() {
	ScreenLevel *level = (ScreenLevel*)Screen::getSelectedScreen();
	if (level == nullptr) return;

	int levelID = level->getLevelNumber();

	ScreenLevel* newLevel = ScreenLevel::loadLevel(levelID + 1);
	if (newLevel == nullptr) {
		glfwSetWindowShouldClose(Squared::getInstance()->getWindow(), true);
		return;
	}

	Screen::setSelectedScreen(newLevel);
}

