#pragma once
#include "GLFW\glfw3.h"
//#include <set>
//#include "Game.h"

class Game;

class Mouse
{
public:
	Mouse(Game *game);

	static void mouseMove(GLFWwindow* window, double x, double y);
	static void mouseButtonEvent(GLFWwindow *window, int button, int action, int mods);
	static void mouseScrollEvent(GLFWwindow* window, double dx, double dy);

	double getExactX();
	double getExactY();

	int getX();
	int getY();

private:
	//double x, y;
	Game* game;
	//std::set<int> buttonsDown;

	static Mouse* getMouse(GLFWwindow* window);
};