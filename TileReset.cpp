#include "stdafx.h"
#include "TileReset.h"

TileReset::TileReset(int x, int y) : Tile(TileType::RESET, x, y) {
	this->solid = false;
	this->resets = true;
}

