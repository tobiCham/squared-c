#include "stdafx.h"
#include "Squared.h"
#include "Screen.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "ScreenLevel.h"
#include "Transition.h"
#include <Windows.h>

#include <stdio.h>  /* defines FILENAME_MAX */
#include <direct.h>

using namespace std;

Squared* Squared::instance = nullptr;
int Squared::FPS = 30;

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd) {

	srand(time(NULL));
	Squared *game = new Squared();
	game->init(1024, 1024, "Squared");

	Image* iconImg = Image::fromFile("resources/icon.img");

	glfwSetWindowIcon(game->getWindow(), 1, iconImg->createGLFWImage());

	game->load();
	game->open();

	delete game;
	cin;
	return 0;
}

Squared::Squared() {
	
}

Squared::~Squared() {
	delete player;
}

void Squared::load() {
	Squared::instance = this;
	loadingImage = Image::fromFile("resources/splash.img");
}

void Squared::onLoaded() {
	render();
	glfwSwapBuffers(getWindow());

	this->player = new Player(this);
	ScreenLevel* level = ScreenLevel::loadLevel(1);
	Screen::setSelectedScreen(level);

	if(FPS == 60) glfwSwapInterval(1);
	else if(FPS == 20) glfwSwapInterval(3);
	else glfwSwapInterval(2);
}

void Squared::setTileSize(int size) {
	if (size < 0) size = 0;
	if (size > getInitialHeight()) size = getInitialHeight();
	if (size > getInitialWidth()) size = getInitialWidth();
	this->tileSize = size;
}

void Squared::render() {
	ColorUtils::setGLColor(ColorUtils::getColor(0, 0, 0));
	DrawUtils::fillRect(0, 0, getInitialWidth(), getInitialHeight());
	if (Screen::getSelectedScreen() != nullptr) Screen::getSelectedScreen()->render();
	else {
		ColorUtils::setGLColor(255, 255, 255);
		glBindTexture(GL_TEXTURE_2D, loadingImage->generateTexture());
		DrawUtils::fillTexture(0, 0, getInitialWidth(), getInitialHeight());
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	Transition::render();
}

void Squared::update() {
	if (Screen::getSelectedScreen() != nullptr) Screen::getSelectedScreen()->update();
	Transition::update();
}
